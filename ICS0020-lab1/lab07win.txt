==== NB! all ansible playbooks were ran from detimil@lab07mgmt


==== ANSIBLE SETUP ====

- on management machine
  >pip install "pywinrm>=0.3.0"

- get the setup script from github
  >$web = New-Object Net.WebClient
  >$web.DownloadFile("https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1", "C:\Users\nimda\ConfigureRemotingForAnsible.ps1")

- execute 
  >powershell -executionpolicy bypass -File C:\Users\nimda\ConfigureRemo
  tingForAnsible.ps1 -Verbose -EnableCredSSP



======= LAB SETUP =======

- check RDP port
  >nc -zv 192.168.180.136 3389
  ouput:
  >Connection to 192.168.180.136 3389 port [tcp/ms-wbt-server] succeeded!

- connect to the lab07win
  >ssh nimda@192.168.180.136

- change password on nimda
  >net user nimda <password>

- create a user directory for detimil
  >New-Item -Path "c:\Users\" -Name "detimil" -ItemType "directory"

- create the user itself
  >net user /add detimil <password>

- enable ssh client
  >Add-WindowsCapability -Online -NAme OpenSSH.Client~~~~0.0.1.0

- make it start up manually
  >Get-Service -name ssh-agent | Set-Service -StartupType Manual

- start the service
  >start-service ssh-agent

- add path to the private key file
  >Get-Service -name ssh-agent | Set-Service -StartupType Manual

- after I generated a key pair on my local machine,
  I copied the public key to .ssh\authorized_keys on nimda
  (also added the public key to C:/ProgramData/ssh/administrators_authorized_keys)

===== CONFIGURE TIME =======

- Configure time
  >C/Windows/system32/tzutil /s "Pacific Standard Time"
  
  >w32tm /debug /enable /file:C:\Windows\Logs\w32tm.log /size:10000000/entries:0-300 /truncate

  >w32tm /resync

 - playbook: lab01_sync_time_win.yaml 
  (resyncs with the time server and extracts the logs)

==== INSTALL WEBSERVER AND LOG====

- playbook: lab01_iis.yaml 
  (installs IIS on lab07win and puts index2.html into C:/inetpub/wwwroot)

- try to interact with the server by going to following addresses: (from lab07mgmt)
  >curl http://192.168.180.136/

  >curl http://192.168.180.136/index2.html

  >curl http://192.168.180.136/index2.htm

- playbook: lab01_iis_log.yaml 
  (collects iis logs into ics0020-labs-outcomes)

=======================
- get logs of password change/reset attempt
  >Get-WinEvent -FilterHashtable @{LogName = 'Security'; ID = 4723,4724}
- get logs of failed login attempts
  >Get-WinEvent -FilterHashtable @{LogName = 'Security'; ID = 4625}
- get logs of successful logins
  >Get-WinEvent -FilterHashtable @{LogName = 'Security'; ID = 4624}
- get log of all services stopped since last boot
  >Get-WinEvent -FilterHashtable @{LogName = 'System'; ID = 7034} | WhereObject {$_.timecreated -gt (Get-CimInstancewin32_Operatingsystem).lastbootuptime}
  
  >Get-WinEvent -FilterHashtable @{LogName = 'System'; ID = 7036; Data ="stopped"} | Where-Object {$_.timecreated -gt (Get-CimInstancewin32_Operatingsystem).lastbootuptime}